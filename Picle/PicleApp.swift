//
//  PicleApp.swift
//  Picle
//
//  Created by TryCatch on 2021/10/01.
//
//

import SwiftUI

@main
struct PicleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
