//  TutorialTestView.swift
//  Picle
//
//  Created by daesung yoon on 2021/10/13.
//

import SwiftUI

struct TutorialView: View {
    // 페이징 초기 넘버 0
    @State private var selection = 0;
    private var numberOfImages = 3
    private let tutoImg = ["0", "1", "2"]
    private let tutoTitle = ["쉽고", "편하고", "자유롭게"]
    private let tutoSubTitle =
    [
        "QR코드와 간단한 잠금장치로 쉽고 편하게",
        "전기모터의 도움으로 편하고 부담없이",
        "대여, 반납은 어디서나 자유롭고 간편하게"
    ]
    var body: some View {
        //튜토리얼 제목,부제목
        //Spacer()
        NavigationView{
            TabView(selection: $selection){
                ForEach(0..<numberOfImages) {
                    num in
                    VStack{
                        Group{
                            Text(tutoTitle[num])
                                .font(.title)
                                .tag("\(num)")
                            Text(tutoSubTitle[num])
                                .tag("\(num)")
                            Image(tutoImg[num])
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(height: 400)
                                .tag("\(num)")
                            
                            //튜토리얼 다음버튼
                            Button(action: {
                                //action
                                withAnimation{
                                    if selection == 0 {
                                        selection = 1
                                    }else if selection == 1 {
                                        selection = 2
                                    }else if selection == 2 {
                                        
                                    } // nmapButton
                                }//withAnimation
                            }) {
                                //display
                                if selection == 0 {
                                    Text("다음")
                                }else if selection == 1 {
                                    Text("다음")
                                }else if selection == 2 {
                                    NavigationLink(destination: NmapView()) {
                                        Text("이용시작")
                                            .foregroundColor(.blue)
                                    }
                                } // nmapButton
                            }
                            
                            // 스킵 버튼
                            Button(
                                action: {
                                    //action
                                }) {
                                    NavigationLink(destination: NmapView()) {
                                        Text("Skip")
                                            .foregroundColor(.blue)
                                    }
                                } // Button
                        } // Group
                    } // Vstack
                } // ForEach
            } // TabView
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(.page(backgroundDisplayMode: .always))
        } // NavigationView
    }// body
} // TutorialTestView

struct TutorialView_Previews: PreviewProvider {
    static var previews: some View {
        // 4
        TutorialTestView()
    }
}
