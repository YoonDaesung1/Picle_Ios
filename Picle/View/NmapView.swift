//
//  NmapView.swift
//  Picle
//
//  Created by TryCatch on 2021/10/12.
//

import Combine
import NMapsMap
import SwiftUI
import UIKit

struct NmapView: View {
    
    // side메뉴가 nMap내부에 표시하기위함
    @State var showMenu = false
    
    var body: some View {
        ZStack{
            // slideBar 스와이프 기능
            let drag = DragGesture()
                .onEnded {
                    if $0.translation.width < -100{
                        withAnimation{
                            self.showMenu = false
                        }
                    }
                }
            
            NavigationView{
                GeometryReader { geometry in
                    MapView(showMenu: self.$showMenu)
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .offset(x: self.showMenu ? geometry.size.width/2 : 1)
                        .disabled(self.showMenu ? true : false)// 가려진화면 기능off
                    
                    if self.showMenu {
                        SideNaviView()
                            .frame(width: geometry.size.width/2)
                            .transition(.move(edge: .leading))
                        
                    }
                }// GeometryReader
                .edgesIgnoringSafeArea(.all)// Nmap 풀스크린 화면속성
            }
                .navigationBarTitle("Picle!!", displayMode: .inline)
                .gesture(drag)// 제스쳐 스와이프
                .navigationBarBackButtonHidden(true)// Back버튼 없애기
                .navigationBarItems(leading: (
                    Button(action: {
                        withAnimation{
                            self.showMenu.toggle()
                        }
                    }) {
                        Image(systemName: "line.horizontal.3")// 좌측 상단 햄버거 로고
                            .imageScale(.large)
                    }
                ))
//                .edgesIgnoringSafeArea(.all) 
            
            VStack{
                Spacer()
                Button(action: {
                    print("action")
                }) {
                    Text("대여하기")
                } // Button
                .frame(width : 340)
                .padding(12)
                .background(Color.blue)
                .cornerRadius(12)
                .foregroundColor(.white)
                .font(Font.body.bold())
            } // VStack
            Spacer()
        } // ZStack
    } // body
} // NmapView

// naverMap 구조체
struct MapView: UIViewRepresentable {
    
    // side메뉴 binding
    @Binding var showMenu: Bool
    
    @ObservedObject var nmapviewModel = NmapViewViewModel()
    func makeUIView(context: Context) -> NMFNaverMapView {
        
        let view = NMFNaverMapView()
        
        // 네이버 지도 마커 띄우기
        let marker = NMFMarker()
        marker.position = NMGLatLng(lat: 37.38545737401266, lng: 127.11959973652351) //마커 위치 임의 값 마커 찍기
        marker.mapView = view.mapView
        marker.iconImage = NMFOverlayImage(name: "orem")
        marker.iconTintColor = UIColor.red
        marker.width = 25
        marker.height = 40
        
        //위치 정보 받아오기(필요없다면 주석)
        view.showZoomControls = true
        view.mapView.positionMode = .direction
        
        //zoom 단계
        view.mapView.zoomLevel = 17
        return view
    } // makeUIView
    
    func updateUIView(_ uiView: NMFNaverMapView, context: Context) {}
} // MapView

struct NmapView_Previews: PreviewProvider {
    static var previews: some View {
        NmapView()
    }
}
