//
//  SideNaviView.swift
//  Picle
//
//  Created by TryCatch on 2021/10/26.
//

import SwiftUI

struct SideNaviView: View {
    var body: some View {
        VStack(alignment: .leading){
            
            HStack{
                Text("OOO 님")
                    .foregroundColor(.gray)
                    .font(.headline)
                Image(systemName: "gear")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                    .frame(alignment: .topTrailing)
            } // HStack
            .padding(.top, 60)
            
            HStack{
                Text("ver 1.0.0")
                    .foregroundColor(.gray)
                    .font(.subheadline)
            } // HStack
            .padding(.top, 20)
            
            HStack{
                Image(systemName: "person")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                
                    NavigationLink(destination: PaymentView()){
                        Text("결제수단")
                            .foregroundColor(.gray)
                            .font(.headline)
                    }//NavigationLink
                    .navigationBarBackButtonHidden(true) // Back버튼 없애기
            }//HStack
            .padding(.top, 20)
            
            HStack{
                Image(systemName: "envelope")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                
                NavigationLink(destination: NoticeView()){
                Text("이용안내")
                    .foregroundColor(.gray)
                    .font(.headline)
                }//NavigationLink
                .navigationBarBackButtonHidden(true) // Back버튼 없애기
            } // HStack
            .padding(.top, 10)
            
            HStack{
                Image(systemName: "gear")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                NavigationLink(destination: UseHistoryView()){
                Text("이용내역")
                    .foregroundColor(.gray)
                    .font(.headline)
                }//NavigationLink
                .navigationBarBackButtonHidden(true) // Back버튼 없애기
            } // HStack
            .padding(.top, 10)
            
            HStack{
                Image(systemName: "person")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                NavigationLink(destination: NoticeView()){
                Text("공지사항")
                    .foregroundColor(.gray)
                    .font(.headline)
                }//NavigationLink
                .navigationBarBackButtonHidden(true) // Back버튼 없애기
            } // HStack
            .padding(.top, 10)
            
            HStack{
                Image(systemName: "gear")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                NavigationLink(destination: CouponListView()){
                Text("쿠폰")
                    .foregroundColor(.gray)
                    .font(.headline)
                }//NavigationLink
                .navigationBarBackButtonHidden(true) // Back버튼 없애기
            } // HStack
            .padding(.top, 10)
            
            HStack{
                Image(systemName: "person")
                    .foregroundColor(.gray)
                    .imageScale(.large)
                Text("로그 아웃")
                    .foregroundColor(.gray)
                    .font(.headline)
            } // HStack
            .padding(.top, 10)
            Spacer()
        } // VStack
        .padding()
        .frame(maxWidth: .infinity, alignment: .leading)
        //.background(Color(red: 32/255, green: 32/255, blue: 32/255))
        .edgesIgnoringSafeArea(.all)
    } // body
} // SideNaviView

struct SideNaviView_Previews: PreviewProvider {
    static var previews: some View {
        SideNaviView()
    }
}
