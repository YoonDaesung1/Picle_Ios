//
//  NoticeView.swift
//  Picle
//
//  Created by TryCatch on 2021/10/27.
//

import SwiftUI

//공지 사항
struct NoticeView: View {
    var body: some View {
        Text("NoticeView")
    }
}

struct NoticeView_Previews: PreviewProvider {
    static var previews: some View {
        NoticeView()
    }
}
