//  TutorialTestView.swift
//  Picle
//
//  Created by daesung yoon on 2021/10/13.
//

import SwiftUI

struct TutorialTestView: View {
    // 페이징 초기 넘버 0
    @State private var selection = 0;
    private var numberOfImages = 3
    private let tutoImg = ["0", "1", "2"]
    private let tutoTitle = ["쉽고", "편하고", "자유롭게"]
    private let tutoSubTitle =
    [
        "QR코드와 간단한 잠금장치로 쉽고 편하게",
        "전기모터의 도움으로 편하고 부담없이",
        "대여, 반납은 어디서나 자유롭고 간편하게"
    ]
    var body: some View {
        //튜토리얼 제목(부제목)
        Spacer()
        TabView(selection: $selection){
            ForEach(0..<numberOfImages) {
                num in
                VStack{
                    Text(tutoTitle[num])
                        .font(.title)
                        .tag("\(num)")
                    Text(tutoSubTitle[num])
                        .tag("\(num)")
                    Image(tutoImg[num])
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .tag("\(num)")
                } // Vstack
            } // ForEach
        } // TabView
        .tabViewStyle(PageTabViewStyle())
        .indexViewStyle(.page(backgroundDisplayMode: .always))
        .frame(height: 450)
        Spacer()
        
        //튜토리얼 다음버튼
        Button(action: {
            //action
            withAnimation{
                if selection == 0 {
                    selection = 1
                }else if selection == 1 {
                    selection = 2
                }else if selection == 2 {
                    
                } // nmapButton
            }//withAnimation
        }) {
            //display
            if selection == 0 {
                Text("다음")
            }else if selection == 1 {
                Text("다음")
            }else if selection == 2 {
                
                Button("이용시작") {
                    
                }
                //Text("이용시작")
                
            } // nmapButton
        }
        
        //        //튜토리얼 다음버튼
        //        Button("다음") {
        //            withAnimation {
        //                if selection == 0 {
        //                    selection = 1
        //                }else if selection == 1 {
        //                    selection = 2
        //                }else if selection == 2 {
        //                } // nmapButton
        //            } // withAnimation
        //        } // Button
        //        .foregroundColor(.blue)
        //        .padding()
        //        .overlay(
        //            RoundedRectangle(cornerRadius: 15)
        //                .stroke(Color.blue, lineWidth: 1)
        //                .frame(width: 100, height: 30)
        //        )
        
        
        //            // MainMap이동 버튼
        //            NavigationView{
        //                NavigationLink(destination: NmapView(),
        //                label:  {
        //                    Text("Nmap")
        //                })
        //                .foregroundColor(.blue)
        //
        //
        //            }  .frame(height: 30)
        //                .listRowInsets(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
        //                .tabViewStyle(PageTabViewStyle())
        //
        
        
    }// body
} // TutorialTestView

struct TutorialTestView_Previews: PreviewProvider {
    static var previews: some View {
        // 4
        TutorialTestView()
    }
}
