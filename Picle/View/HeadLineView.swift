//
//  HeadLine.swift
//  Picle
//
//  Created by TryCatch on 2021/10/26.
//

import SwiftUI

struct HeadLine: View {
    var body: some View {
        Text("Picle")
        .padding(.top, 10)
        .font(.headline)
        .foregroundColor(.blue)
    }
}

