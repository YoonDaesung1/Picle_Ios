//
// Created by TryCatch on 2021/10/01.
//
import SwiftUI

struct HomeView: View {

    var body: some View {
        NavigationView {
            List(0..<5) { i in
                Text("Item - \(i)")
            }.navigationBarTitle("My List")
        }
    }

}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}