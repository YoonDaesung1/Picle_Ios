import SwiftUI

@main
struct AwesomeSplashApp: App {
    var body: some Scene {
        WindowGroup {
            SplashView()
        }
    }
}